# OpenML dataset: bfi_dataset

https://www.openml.org/d/43146

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset description**

25 personality self report items taken from the International Personality Item Pool (ipip.ori.org) were included as part of the Synthetic Aperture Personality Assessment (SAPA) web based personality assessment project. The data from 2800 subjects are included here as a demonstration set for scale construction, factor analysis, and Item Response Theory analysis. Three additional demographic variables (sex, education, and age) are also included.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43146) of an [OpenML dataset](https://www.openml.org/d/43146). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43146/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43146/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43146/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

